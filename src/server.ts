import { Request, Response } from "express";
import express from "express";

const app = express();

app.get("/", (request: Request, response: Response) => {
  return response.json({
    message: "hello express!",
  });
});

app.listen(3333);
